const { FighterRepository } = require("../repositories/fighterRepository")

class FighterService {
  // TODO: Implement methods to work with fighters

  findFighters() {
    const fighters = FighterRepository.getAll()
    if (!fighters) {
      throw Error("Fighters not found")
    }
    return fighters
  }

  findFighterById(id) {
    if (!id) {
      throw new Error("Didn`t match id")
    }
    const fighter = this.search({ id })
    if (!fighter) {
      throw Error("Fighter not found")
    }
    return fighter
  }

  createFighter(data) {
    const candidateName = FighterRepository.getOne({ name: data.name })
    if (candidateName) {
      throw new Error("Such fighter name already exists")
    } else {
      const fighter = FighterRepository.create(data)
      return fighter
    }
  }

  updateFighter(id, data) {
    const fighter = FighterRepository.getOne({ id })
    if (!fighter) {
      throw new Error("Fighter not found")
    }
    const updateFighter = FighterRepository.update(id, data)
    if (!updateFighter) {
      throw Error("Fighter didn`t update")
    }
    return updateFighter
  }

  deleteFighter(id) {
    const fighter = FighterRepository.getOne({ id })
    if (!fighter) {
      throw new Error(`Fighter with id ${id} not found`)
    }
    const deleteFighter = FighterRepository.delete(id)
    if (!deleteFighter) {
      throw Error("Fighter didn`t delete")
    }
    return deleteFighter
  }

  search(search) {
    const fighterSearch = FighterRepository.getOne(search)

    if (!fighterSearch) {
      return null
    }
    return fighterSearch
  }
}

module.exports = new FighterService()