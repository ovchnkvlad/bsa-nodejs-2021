const { FightRepository } = require("../repositories/fightRepository")

class FightService {
  // OPTIONAL TODO: Implement methods to work with fights
  findFights() {
    const fights = FightRepository.getAll()
    if (!fights) {
      throw Error("Fights not found")
    }
    return fights
  }

  findFightById(id) {
    if (!id) {
      throw new Error("Didn`t match id")
    }
    const fight = this.search({ id })
    if (!fight) {
      throw Error("Fight not found")
    }
    return fight
  }

  deleteFight(id) {
    const fight = FightRepository.getOne({ id })
    if (!fight) {
      throw new Error(`Fight with id ${id} not found`)
    }
    const deleteFight = FightRepository.delete(id)
    if (!deleteFight) {
      throw Error("Fight didn`t delete")
    }
    return FightRepository.delete(id)
  }
}

module.exports = new FightService()