const { UserRepository } = require("../repositories/userRepository")

class UserService {
  // TODO: Implement methods to work with user

  findUsers() {
    const users = UserRepository.getAll()
    if (!users) {
      throw Error("Users not found")
    }
    return users
  }

  findUserById(id) {
    if (!id) {
      throw new Error("Didn`t match id")
    }
    const user = this.search({ id })
    if (!user) {
      throw Error("User not found")
    }
    return user
  }

  createUser(data) {
    const candidateEmail = UserRepository.getOne({ email: data.email })
    const candidatePhone = UserRepository.getOne({
      phoneNumber: data.phoneNumber,
    })

    if (candidateEmail || candidatePhone) {
      throw new Error("Such email or phone already exists")
    } else {
      const user = UserRepository.create(data)
      return user
    }
  }

  updateUser(id, data) {
    const user = UserRepository.getOne({ id })
    if (!user) {
      throw new Error("User not found")
    }
    const updateUser = UserRepository.update(id, data)
    if (!updateUser) {
      throw Error("User didn`t update")
    }
    return updateUser
  }

  deleteUser(id) {
    const user = UserRepository.getOne({ id })
    if (!user) {
      throw new Error(`User with id ${id} not found`)
    }
    const deleteUser = UserRepository.delete(id)
    if (!deleteUser) {
      throw Error("User didn`t delete")
    }
    return deleteUser
  }

  search(search) {
    const item = UserRepository.getOne(search)
    if (!item) {
      return null
    }
    return item
  }
}

module.exports = new UserService()