const { Router } = require("express")
const authController = require("../controllers/authController")
const { responseMiddleware } = require("../middlewares/response.middleware")

const router = Router()

router.post("/login", authController.login, responseMiddleware)

module.exports = router