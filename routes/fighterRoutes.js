const { Router } = require("express")
const fighterController = require("../controllers/fighterController")
const { responseMiddleware } = require("../middlewares/response.middleware")
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware")

const router = Router()

// TODO: Implement route controllers for fighter

router.get("/", fighterController.getAllFighters, responseMiddleware)

router.get("/:id", fighterController.getOneFighter, responseMiddleware)

router.post(
  "/",
  createFighterValid,
  fighterController.createFighter,
  responseMiddleware
)

router.put(
  "/:id",
  updateFighterValid,
  fighterController.updateFighter,
  responseMiddleware
)

router.delete("/:id", fighterController.deleteFighter, responseMiddleware)

module.exports = router