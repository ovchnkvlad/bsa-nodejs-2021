const { Router } = require("express")
const fightController = require("../controllers/fightController")
const { responseMiddleware } = require("../middlewares/response.middleware")

const router = Router()

// OPTIONAL TODO: Implement route controller for fights

router.get("/", fightController.getAllFights, responseMiddleware)

router.get("/:id", fightController.getFight, responseMiddleware)

router.delete("/:id", fightController.deleteFight, responseMiddleware)

module.exports = router