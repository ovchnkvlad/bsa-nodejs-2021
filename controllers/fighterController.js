const FighterService = require("../services/fighterService")

module.exports = {
  getAllFighters: async (req, res, next) => {
    try {
      const fighters = await FighterService.findFighters()
      res.data = fighters
      res.status(200).json(fighters)
    } catch (e) {
      res.status(404).json({ error: true, message: "Fighters not found!" })
    } finally {
      next()
    }
  },

  getOneFighter: async (req, res, next) => {
    try {
      const { id } = req.params
      if (!id) {
        return res
          .status(404)
          .json({ error: true, message: "Fighter not found!" })
      }
      const fighter = await FighterService.findFighterById(id)
      res.data = fighter
      res.status(200).json(fighter)
    } catch (e) {
      res.status(404).json({ error: true, message: "Fighter not found!" })
    } finally {
      next()
    }
  },

  createFighter: async (req, res, next) => {
    try {
      const newFighter = await FighterService.createFighter(req.body)
      res.data = newFighter
      res.status(200).json("Fighter created!")
    } catch (e) {
      res.status(400).json({ error: true, message: "Fighter not created!" })
    } finally {
      next()
    }
  },

  updateFighter: async (req, res, next) => {
    try {
      const { id } = req.params
      if (!id) {
        return res
          .status(404)
          .json({ error: true, message: "Fighter not found!" })
      }
      const updateFighter = await FighterService.updateFighter(id, req.body)
      res.data = updateFighter
      res.status(200).json("Fighter updated!")
    } catch (e) {
      res.status(400).json({ error: true, message: "Fighter not updated!" })
    } finally {
      next()
    }
  },

  deleteFighter: async (req, res, next) => {
    try {
      const { id } = req.params
      if (!id) {
        return res
          .status(404)
          .json({ error: true, message: "Fighter not found!" })
      }
      const deleteFighter = await FighterService.deleteFighter(id)
      res.data = deleteFighter
      res.status(200).json("Fighter deleted!")
    } catch (e) {
      res.status(404).json({ error: true, message: "Fighter not deleted!" })
    } finally {
      next()
    }
  },
}