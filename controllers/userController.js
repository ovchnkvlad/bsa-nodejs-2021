const UserService = require("../services/userService")

module.exports = {
  getAllUsers: async (req, res, next) => {
    try {
      const users = await UserService.findUsers()
      res.data = users
      res.status(200).json(users)
    } catch (e) {
      res.status(404).json({ error: true, message: "Users not found!" })
    } finally {
      next()
    }
  },

  getOneUser: async (req, res, next) => {
    try {
      const { id } = req.params
      if (!id) {
        return res.status(404).json("User not found!")
      }
      const user = await UserService.findUserById(id)
      res.data = user
      return res.status(200).json(user)
    } catch (e) {
      res.status(404).json({ error: true, message: "User not found!" })
    } finally {
      next()
    }
  },

  createUser: async (req, res, next) => {
    try {
      const newUser = await UserService.createUser(req.body)
      res.data = newUser
      return res.status(200).json("User created!")
    } catch (e) {
      res.status(400).json({ error: true, message: "User not created!" })
    } finally {
      next()
    }
  },

  updateUser: async (req, res, next) => {
    try {
      const { id } = req.params
      if (!id) {
        return res.status(404).json({ error: true, message: "User not found!" })
      }
      const updateUser = await UserService.updateUser(id, req.body)
      res.data = updateUser
      return res.status(200).json("User updated!")
    } catch (e) {
      res.status(400).json({ error: true, message: "User not updated!" })
    } finally {
      next()
    }
  },

  deleteUser: async (req, res, next) => {
    try {
      const { id } = req.params
      if (!id) {
        return res.status(404).json({ error: true, message: "User not found!" })
      }
      const deleteUser = await UserService.deleteUser(id)
      res.data = deleteUser
      return res.status(200).json("User deleted!")
    } catch (e) {
      res.status(404).json({ error: true, message: "User not deleted!" })
    } finally {
      next()
    }
  },
}