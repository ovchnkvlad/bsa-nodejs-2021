const AuthService = require("../services/authService")

module.exports = {
  login: async (req, res, next) => {
    try {
      // TODO: Implement login action (get the user if it exist with entered credentials)
      const { email, password } = req.body
      const user = await AuthService.login({ email, password })
      res.data = user
      if (!user) {
        throw {
          message: "User not found",
          status: 404,
        }
      }
    } catch (err) {
      res.status(404).json({ error: true, message: "User not found!" })
    } finally {
      next()
    }
  },
}