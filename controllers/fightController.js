const FightService = require("../services/fightService")

module.exports = {
  getAllFights: async (req, res, next) => {
    try {
      const fights = await FightService.findFights()
      res.data = fights
      res.status(200).json(fights)
    } catch (e) {
      res.status(404).json({ error: true, message: "Fights not found!" })
    } finally {
      next()
    }
  },

  getFight: async (req, res, next) => {
    try {
      const { id } = req.params
      if (!id) {
        return res
          .status(404)
          .json({ error: true, message: "Fight not found!" })
      }
      const fight = await FightService.findFightById(req.params.id)
      res.data = fight
      res.status(200).json(fight)
    } catch (e) {
      res.status(404).json({ error: true, message: "Fight not found!" })
    } finally {
      next()
    }
  },

  deleteFight: async (req, res, next) => {
    try {
      const { id } = req.params
      if (!id) {
        return res
          .status(404)
          .json({ error: true, message: "Fight not found!" })
      }
      const deleteFight = await FightService.deleteFight(req.params.id)
      res.data = deleteFight
      res.status(200).json("Fight deleted!")
    } catch (e) {
      res.status(404).json({ error: true, message: "Fight not deleted!" })
    } finally {
      next()
    }
  },
}