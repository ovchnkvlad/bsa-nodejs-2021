const { fighter } = require("../models/fighter")
const validationService = require("../services/validationService")
const FighterService = require("../services/fighterService")
const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  try {
    const { name, power, defense, health } = req.body
    if (!req.body) {
      throw new Error("Request body mustn`t be null")
    }
    if (!validationService.isNumber(power, 1, 100)) {
      throw new Error("Power must be between 1 and 100")
    }
    if (!validationService.isNumber(defense, 1, 10)) {
      throw new Error("Defence must be between 1 and 10")
    }
    if (!validationService.isNumber(health, 80, 120)) {
      throw new Error("Health must be between 80 and 120")
    }
    if (!validationService.isString(name)) {
      throw new Error("Name isn't valid")
    }
  } catch (e) {
    res.status(400).json(e.message)
  } finally {
    next()
  }
}

const updateFighterValid = async (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  const { name, power, defense, health } = req.body
  try {
    if (!req.body) {
      throw new Error("Request body mustn`t be null")
    }
    for (let key in req.body) {
      if (!fighter.hasOwnProperty(key)) {
        throw new Error("Fighter data to update isn't valid")
      }
    }
    if (name && !validationService.isString(name)) {
      throw new Error("Name is not valid")
    }
    if (health && !validationService.isNumber(health, 80, 120)) {
      throw new Error("Health must be between 80 and 120")
    }
    if (defense && !validationService.isNumber(defense, 1, 10)) {
      throw new Error("Defence must be between 1 and 10")
    }
    if (power && !validationService.isNumber(power, 1, 100)) {
      throw new Error("Power must be between 1 and 100")
    }
  } catch (e) {
    res.status(400).json({ error: true, message: "Fighter data is not valid" })
  } finally {
    next()
  }
}
exports.createFighterValid = createFighterValid
exports.updateFighterValid = updateFighterValid